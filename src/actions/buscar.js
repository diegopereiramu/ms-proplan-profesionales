import {
    buscar as buscarProfesionales, buscarPorCentrosCostos,
    listadoProfesionales as listadoPro
} from '@repository/profesionales';
import { mapToProfesionales } from '@helpers/utils';

const buscar = async (req, res, next) => {
    let profesionalesDb = [];

    if (req.query.centrosCostos !== undefined) {
        profesionalesDb = await buscarPorCentrosCostos(req.query.centrosCostos.split(','), req.query.idPeriodo);
    } else {
        profesionalesDb = await buscarProfesionales(true, req.query.idPeriodo);
    }

    res.status(200).json({
        OK: true,
        data: mapToProfesionales(profesionalesDb)
    });
};
const listarProfesional = async (req, res, next) => {
    let profesionalesDb = [];
    try {
        profesionalesDb = await listadoPro();
        res.status(200).json({
            OK: true,
            data: profesionalesDb
        });
    } catch (error) {
        res.status(200).json({
            OK: false,
            data: profesionalesDb
        });
    }
    
};

const buscarPorCC = async (req, res, next) => {
    const profesionales =req.body;
    try {
        const profesionalist = await buscarPorCentrosCostos(profesionales);
        res.status(200).json({
            OK: true,
            data: profesionalist
        });
    } catch (error) {
        res.status(200).json({
            OK: false,
            data: profesionalist
        });
    }

};
export {
    buscar,
    buscarPorCC,
    listarProfesional
};
