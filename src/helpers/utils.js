const mapToProfesionales = (profesionalesDb) => {
    let profesionales = [];
    let profesional = {};
    let iProfesionalExistente = 0;
    let titulo = {};
    let ley = "";
    profesionalesDb.forEach(profesionalDb => {
        profesional =  {
            id: profesionalDb.id,
            nombre: profesionalDb.nombre,
            documento:{
                numero: profesionalDb.documento,
                tipo: profesionalDb.tipo_documento
            },
            titulos: [],
            activo: profesionalDb.activo,
            tipo_actividad: profesionalDb.especialidad_sis != null ? 'M' : 'N',
            leyes: [],
        };
        iProfesionalExistente = profesionales.findIndex(p => p.id == profesional.id);
        titulo = {
            id: profesionalDb.titulo_id,
            descripcion: profesionalDb.titulo
        };
        ley = profesionalDb.ley_codigo;

        if(iProfesionalExistente < 0){
            profesional.titulos.push(titulo);
            profesional.leyes.push(ley);
            profesionales.push(profesional);
        }else{
            if(profesionales[iProfesionalExistente].tipo_actividad != profesional.tipo_actividad){
                profesionales[iProfesionalExistente].tipo_actividad = 'A';
            }
            if(profesionales[iProfesionalExistente].titulos.findIndex(t => t.id == titulo.id) < 0){
                profesionales[iProfesionalExistente].titulos.push(titulo);
            }
            if(profesionales[iProfesionalExistente].leyes.indexOf(ley)<0){
                profesionales[iProfesionalExistente].leyes.push(ley);
            }
        }
    });
    
    return profesionales;
};

export {
    mapToProfesionales,
};