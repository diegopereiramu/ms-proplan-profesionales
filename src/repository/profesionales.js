import { query } from './pg.connect';

const buscar = async (activo, idPeriodo) => {
    try {
        const q = `
        SELECT 
            p.id,
            p.nombre, 
            p.documento,             
            td.codigo as tipo_documento,
            p.activo,
            t.id as titulo_id,
            t.descripcion as titulo,
            c.especialidad_sis,
            l.codigo as ley_codigo            
        FROM
            "proplan"."profesional" p
        INNER JOIN
            "proplan"."tipo_documento" td on td.id = p.tipo_documento_id
        INNER JOIN
            "proplan"."contrato" c on c.profesional_id = p.id
        LEFT JOIN
            "proplan"."profesional_titulo" pt on pt.profesional_id = p.id
        LEFT JOIN
            "proplan"."titulo" t on t.id = pt.titulo_id
        ${idPeriodo != null ? 'INNER' : 'LEFT'} JOIN
            "proplan"."periodo" pe on pe.id = $1 and ( c.fecha_inicio between pe.fecha_inicio and pe.fecha_termino)
        LEFT JOIN
            "proplan"."ley" l on l.id = c.ley_id;`;
        
        const result = await query(q, [idPeriodo]);
        return result.rows;
    } catch (error) {
        throw error;
    }
};

const buscarPorCentrosCostos = async (centrosCostos, idPeriodo) => {
    centrosCostos = centrosCostos.map(Number);
    try {
        const q = `
        SELECT 
            p.id,
            p.nombre, 
            p.documento,             
            td.codigo as tipo_documento,
            p.activo,
            t.id as titulo_id,
            t.descripcion as titulo,
            c.especialidad_sis,
            l.codigo as ley_codigo
        FROM
            "proplan"."profesional" p
        INNER JOIN
            "proplan"."tipo_documento" td on td.id = p.tipo_documento_id
        INNER JOIN
            "proplan"."contrato" c on c.profesional_id = p.id
        INNER JOIN
            "proplan"."contrato_centro_costo" ccc on ccc.contrato_id = c.id and ccc.centro_costo_id = ANY ($1::int[])
        LEFT JOIN
            "proplan"."profesional_titulo" pt on pt.profesional_id = p.id
        LEFT JOIN
            "proplan"."titulo" t on t.id = pt.titulo_id
        ${idPeriodo != null ? 'INNER' : 'LEFT'} JOIN
            "proplan"."periodo" pe on pe.id = $2 and (c.fecha_termino is null or pe.fecha_inicio <= c.fecha_termino)
        LEFT JOIN 
            "proplan"."ley" l on l.id = c.ley_id
        WHERE c.fecha_inicio between pe.fecha_inicio and pe.fecha_termino;`;

        const result = await query(q,[centrosCostos, idPeriodo]);
        return result.rows;
    } catch (error) {
        throw error;
    }
};

const listadoProfesionales = async () => {
    try {
        const q = `
        SELECT 
            p.id,
            p.documento,
            p.tipo_documento_id,
            p.nombre,
            p.activo        
        FROM
            "proplan"."profesional" p;`;
        const result = await query(q);
        return result.rows;
    } catch (error) {
        throw error;
    }
};
export { 
    buscar,
    buscarPorCentrosCostos,
    listadoProfesionales,
};
