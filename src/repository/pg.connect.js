const { Client } = require('pg')
const util = require('util');
const connectionData = {
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: 'progra0411',
    port: 5432,
}
const client = new Client(connectionData)
client.connect()

const query = util.promisify(client.query).bind(client);

export {
    query
};
