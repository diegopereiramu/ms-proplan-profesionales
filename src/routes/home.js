import { buscar, listarProfesional} from '@actions/buscar';

export default (router, app) => {
    router.get('/', buscar);
    router.get('/listado', listarProfesional);
};